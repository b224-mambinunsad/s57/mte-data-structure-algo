let collection = [];

// Write the queue functions below.
function print() {
	return collection
};

function enqueue(text){
	collection.push(text)
	return collection
};

function dequeue(){
	collection.shift()
	return collection

};

function front(){
	let first = collection[0]
	return first
};

function size(){
	return collection.length
};

function isEmpty() {
	if(collection.length == 0){
		return true
	}else{
		return false
	}
};


module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};